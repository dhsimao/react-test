React Ui-Material Admin Template
================================

Installation / Running
----------------------

Após baixar o projeto:

1. Acesse a pasta: 'cd react-test';
2. Instale as dependências: 'npm install';
3. Inicie a aplicação: 'npm start';
4. Acesse o navegador no endereço: [http://localhost:3000].
