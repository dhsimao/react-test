import React from 'react';
import {cyan600, pink600, purple600, orange600} from 'material-ui/styles/colors';
import Assessment from 'material-ui/svg-icons/action/assessment';
import AddMap from 'material-ui/svg-icons/maps/add-location';
import Edit from 'material-ui/svg-icons/image/edit';
import NoteAdd from 'material-ui/svg-icons/action/note-add';
import InfoBox from '../components/dashboard/InfoBox';
import GraphEx from '../components/dashboard/GraphEx';
import RecentlyGuides from '../components/dashboard/RecentlyGuides';
import globalStyles from '../styles';
import Data from '../data';

const DashboardPage = () => {

  return (
    <div>
      <h3 style={globalStyles.navigation}>Painel de Controle</h3>

      <div className="row">

        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={NoteAdd}
                   color={pink600}
                   title="Cadastrar Guia"
                   value=""
          />
        </div>


        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={AddMap}
                   color={cyan600}
                   title="Cadastrar Pontos"
                   value=""
          />
        </div>

        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={Assessment}
                   color={purple600}
                   title="Gerar Relatórios"
                   value=""
          />
        </div>

        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={Edit}
                   color={orange600}
                   title="Editar Cadastro"
                   value=""
          />
        </div>
      </div>

      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
          <RecentlyGuides data={Data.dashBoardPage.recentGuides}/>
        </div>

        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
          <GraphEx data={Data.dashBoardPage.graphEx}/>
        </div>
      </div>
    </div>
  );
};

export default DashboardPage;
