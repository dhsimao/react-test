import React from 'react';
import {Link} from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import {grey400} from 'material-ui/styles/colors';
import PageBase from '../components/PageBase';

const FormPage = () => {

  const styles = {
    toggleDiv: {
      maxWidth: 300,
      marginTop: 40,
      marginBottom: 5
    },
    toggleLabel: {
      color: grey400,
      fontWeight: 100
    },
    buttons: {
      marginTop: 30,
      float: 'right'
    },
    saveButton: {
      marginLeft: 5
    }
  };

  return (
    <PageBase title="Guia de Trânsito Animal"
              navigation="Formulário">
      <form>

        <TextField
          hintText="NomeR"
          floatingLabelText="Nome do Remetente"
          fullWidth={true}
        />
        <TextField
          hintText="NomeD"
          floatingLabelText="Nome do Destinatário"
          fullWidth={true}
        />
        <TextField
          hintText="PP"
          floatingLabelText="Ponto de Partida"
          fullWidth={true}
        />
        <TextField
          hintText="PC"
          floatingLabelText="Ponto de Chegada"
          fullWidth={true}
        />
        <TextField
          hintText="Contato"
          floatingLabelText="Contato do Destinatário"
          fullWidth={true}
        />

        <TextField
          hintText="Animal"
          floatingLabelText="Animal a ser transportado"
          fullWidth={true}
        />

        <TextField
          hintText="Quantidade"
          floatingLabelText="Quantidade"
          fullWidth={true}
        />

        <DatePicker
          hintText="Valor"
          floatingLabelText="Valor"
          fullWidth={true}/>


        <div style={styles.buttons}>
          <Link to="/">
            <RaisedButton label="Cancelar"/>
          </Link>

          <RaisedButton label="Salvar"
                        style={styles.saveButton}
                        type="submit"
                        primary={true}/>
        </div>
      </form>
    </PageBase>
  );
};

export default FormPage;
