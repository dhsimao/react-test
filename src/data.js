import React from 'react';
import Assessment from 'material-ui/svg-icons/action/assessment';
import GridOn from 'material-ui/svg-icons/image/grid-on';
import PermIdentity from 'material-ui/svg-icons/action/perm-identity';
import Map from 'material-ui/svg-icons/maps/map';
import Web from 'material-ui/svg-icons/av/web';
import {cyan600, pink600, purple600} from 'material-ui/styles/colors';
import ExpandLess from 'material-ui/svg-icons/navigation/expand-less';
import ExpandMore from 'material-ui/svg-icons/navigation/expand-more';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';

const data = {
  menus: [
    { text: 'Painel de Controle', icon: <Assessment/>, link: '/dashboard' },
    { text: 'Formulário', icon: <Web/>, link: '/form' },
    { text: 'Tabela', icon: <GridOn/>, link: '/table' },
    { text: 'Mapas de Rotas', icon: <Map/> },
    { text: 'Login', icon: <PermIdentity/>, link: '/login' }
  ],
  tablePage: {
    items: [
      {id: 1, name: 'Guia 1', price: 'R$50.00', category: 'Categoria A'},
      {id: 2, name: 'Guia 2', price: 'R$50.00', category: 'Categoria A'},
      {id: 3, name: 'Guia 3', price: 'R$50.00', category: 'Categoria A'},
      {id: 4, name: 'Guia 4', price: 'R$50.00', category: 'Categoria A'},
      {id: 5, name: 'Guia 5', price: 'R$70.00', category: 'Categoria B'},
      {id: 6, name: 'Guia 6', price: 'R$70.00', category: 'Categoria B'},
      {id: 7, name: 'Guia 7', price: 'R$70.00', category: 'Categoria B'},
      {id: 8, name: 'Guia 8', price: 'R$70.00', category: 'Categoria B'}
    ]
  },
  dashBoardPage: {
    recentGuides: [
      {id: 1, title: 'Guia 098 - Foz do Iguaçu-> Curitiba', text: 'Data: 12/09/2018'},
      {id: 2, title: 'Guia 097 - Londrina -> Toledo', text: 'Data:10/07/2018'},
      {id: 3, title: 'Guia 096 - Jacerézinho -> Guarapuava', text: 'Data: 02/06/2018'},
      {id: 4, title: 'Guia 095 - Pato Branco -> Bandeirantes', text: 'Data:23/04/2018'}
    ],
    graphEx: [
      {name: 'Data 01', value: 800, color: cyan600, icon: <ExpandMore/>},
      {name: 'Data 02', value: 300, color: pink600, icon: <ChevronRight/>},
      {name: 'Data 03', value: 300, color: purple600, icon: <ExpandLess/>}
    ]
  }
};

export default data;
