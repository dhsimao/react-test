// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import LocationOn from "@material-ui/icons/LocationOn";
import DashboardPage from "views/Dashboard/Dashboard.jsx";
import FormGta from "views/FormGta/FormGta.jsx";
import TableList from "views/TableList/TableList.jsx";
import Maps from "views/Maps/Maps.jsx";

const dashboardRoutes = [
  {
    path: "/dashboard",
    sidebarName: "Painel de Controle",
    navbarName: "Painel de Controle",
    icon: Dashboard,
    component: DashboardPage
  },
  {
    path: "/form",
    sidebarName: "Formulário - GTA",
    navbarName: "Guia de Transporte Animal",
    icon: "content_paste",
    component: FormGta
  },
  {
    path: "/table",
    sidebarName: "Guias e Pontos",
    navbarName: "Guias e Pontos",
    icon: "content_paste",
    component: TableList
  },
  {
    path: "/maps",
    sidebarName: "Mapa",
    navbarName: "Mapa",
    icon: LocationOn,
    component: Maps
  },
  { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default dashboardRoutes;
