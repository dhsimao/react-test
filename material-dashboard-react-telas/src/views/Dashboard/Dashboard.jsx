import React from "react";
import PropTypes from "prop-types";
import ChartistGraph from "react-chartist";
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
import Money from "@material-ui/icons/Money";
import Assessment from "@material-ui/icons/Assessment";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Table from "components/Table/Table.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import AddMap from "@material-ui/core/SvgIcon/maps/add-location"

import {
  guiasEmitidas,
  animaisMovimentados,
  valoresInvestidos
} from "variables/charts.jsx";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";

class Dashboard extends React.Component {
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="warning" stats icon>
                <CardIcon color="warning">
                  <Icon>content_copy</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Guias</p>
                <h3 className={classes.cardTitle}>
                  10
                </h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                    Consulte as guias solicitadas
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="success" stats icon>
                <CardIcon color="success">
                  <AddMap/>
                </CardIcon>
                <p className={classes.cardCategory}>Pontos</p>
                <h3 className={classes.cardTitle}>20</h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  Adicione e edite novos pontos
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="primary" stats icon>
                <CardIcon color="primary">
                  <Assessment/>
                </CardIcon>
                <p className={classes.cardCategory}>Relatórios</p>
                <h3 className={classes.cardTitle}>5</h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  Gere relatórios diversos
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="info" stats icon>
                <CardIcon color="info">
                  <Money />
                </CardIcon>
                <p className={classes.cardCategory}>Contas</p>
                <h3 className={classes.cardTitle}>01</h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  Consulte os boletos pendentes
                </div>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <Card chart>
              <CardHeader color="success">
                <ChartistGraph
                  className="ct-chart"
                  data={guiasEmitidas.data}
                  type="Line"
                  options={guiasEmitidas.options}
                  listener={guiasEmitidas.animation}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>Guias Emitidas</h4>
              </CardBody>
              <CardFooter chart>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card chart>
              <CardHeader color="warning">
                <ChartistGraph
                  className="ct-chart"
                  data={animaisMovimentados.data}
                  type="Bar"
                  options={animaisMovimentados.options}
                  responsiveOptions={animaisMovimentados.responsiveOptions}
                  listener={animaisMovimentados.animation}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>Animais movimentados</h4>
              </CardBody>
              <CardFooter chart>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card chart>
              <CardHeader color="info">
                <ChartistGraph
                  className="ct-chart"
                  data={valoresInvestidos.data}
                  type="Line"
                  options={valoresInvestidos.options}
                  listener={valoresInvestidos.animation}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>Valores investidos</h4>
              </CardBody>
              <CardFooter chart>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
         <GridItem xs={12} sm={12} md={6}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Guias de Transporte</h4>
                <p className={classes.cardCategoryWhite}>
                  Veja as guias cadastradas recentemente
                </p>
              </CardHeader>
              <CardBody>
                <Table
                  tableHeaderColor="primary"
                  tableHead={["ID", "Origem", "Destino", "Finalidade"]}
                  tableData={[
                    ["01", "Londrina", "Foz do Iguaçu", "Abate"],
                    ["02", "Curitiba", "Paranaguá", "Exposição"],
                    ["03", "Morretes", "Jacarézinho", "Abate"],
                    ["04", "Ponta Grossa", "Toledo", "Abate"]
                  ]}
                />
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={6}>
            <Card>
              <CardHeader color="warning">
                <h4 className={classes.cardTitleWhite}>Pontos Cadastrados</h4>
                <p className={classes.cardCategoryWhite}>
                  Veja os pontos cadastrados recentemente
                </p>
              </CardHeader>
              <CardBody>
                <Table
                  tableHeaderColor="warning"
                  tableHead={["ID", "Nome", "Cidade", "Estado"]}
                  tableData={[
                    ["01", "Fazenda Liberdade", "Morretes", "Paraná"],
                    ["02", "Frigirífico Almeida", "Londrina", "Paraná"],
                    ["03", "Safira Alimentos", "Jacarézinho", "Paraná"],
                    ["04", "Schultz Frios", "Toledo", "Paraná"]
                  ]}
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
