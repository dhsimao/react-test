import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Table from "components/Table/Table.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";



const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

function TableList(props) {
  const { classes } = props;
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>Guias de Transporte</h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["Código", "Origem", "Destino", "Data"]}
              tableData={[
                ["0001", "Londrina - PR", "Campo Mourão - PR", "02/11/2018"],
                ["0002", "Curitiba - PR", "Toledo - PR", "29/10/2018"],
                ["0003", "Foz do Iguaçu - PR", "Paranagua - PR", "12/10/2018"],
                ["0004", "Cascavel - PR", "Apucarana - PR", "15/09/2018"],
                ["0005", "Pato Branco - PR", "Cornélio Procópio - PR", "03/09/2018"],
                ["0006", "Maringá - PR", "Jacarézinho- PR", "27/08/2018"]
              ]}
            />
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={12}>
        <Card plain>
          <CardHeader plain color="success">
            <h4 className={classes.cardTitleWhite}>
              Pontos Cadastrados
            </h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["Código", "Nome do local", "Cidade", "Estado", "País"]}
              tableData={[
                ["0001", "Fazenda Liberdade", "Ivaiporã", "Paraná", "Brasil"],
                ["0002", "Sofia Alimentos", "Morretes", "Paraná", "Brasil"],
                ["0003", "Frigorífico Silva", "Ibaiti", "Paraná", "Brasil"],
                [
                  "0004",
                  "Fazenda Luz",
                  "Toledo",
                  "Paraná",
                  "Brasil"
                ],
                [
                  "0005",
                  "Saad Frios",
                  "Londrina",
                  "Paraná",
                  "Brasil"
                ],
                ["0006", "Fazenda Recanto Alegre", "Ponta Grossa", "Paraná", "Brasil"]
              ]}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
}

export default withStyles(styles)(TableList);
